package com.custom.java;

public class MyTuple {
	private int tupleId;
	private int columnNumber;
	private String value;
	private String trueClass;
	public MyTuple(int tupleId, int columnNumber, String value, String trueClass) {
		this.tupleId = tupleId;
		this.columnNumber = columnNumber;
		this.value = value;
		this.trueClass = trueClass;
	}
	
	public int getTupleId(){
		return tupleId;
	}
	
	public int getColumnNumber() {
		return columnNumber;
	}
	
	public String getValue() {
		return value;
	}
	
	public String getTrueClass() {
		return trueClass;
	}
	
	@Override
	public String toString() {
		return "TupleId: " + tupleId + ", ColumnNumber: " + columnNumber + ", Value: " + value + ", Class: " + trueClass;
	}
}
