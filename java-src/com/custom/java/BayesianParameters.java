package com.custom.java;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

public class BayesianParameters {

	/**
	 * Column - <Value - <Class - Probability> > 
	 */
	private static Map<Integer, Map<String, Map<String, Double>>> parametes = new HashMap<Integer, Map<String, Map<String, Double>>>();
	
	static {
		populateInitialMap();
	}
	public static Map<Integer, Map<String, Map<String, Double>>> getParameters() {
		return parametes;
	}
	
	private static void populateInitialMap() {
		String fileLocation = "/Users/rohan/Documents/streambase/AdaptiveNaiveBayesian/java-src/com/custom/java/loadInitialParameters.txt";
		File file = new File(fileLocation);
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
			String str = "";
			while((str = bufferedReader.readLine()) != null) {
				String[] values = str.split("\t");
				setParameter(Integer.parseInt(values[0]), values[1], values[2], Double.parseDouble(values[3]));
			}
			bufferedReader.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}

	public static void setParameter(Map<Integer, Map<String, Map<String, Double>>> newParametes ) {
		parametes = newParametes;
	}
	
	
	private static void setParameter(int columnNumber, String value, String classValue, double probability) {
		Map<String, Map<String, Double>> columnValueMap = parametes.get(columnNumber);
		if(columnValueMap == null) {
			columnValueMap = new HashMap<String, Map<String, Double>>();
		}
		
		Map<String, Double> valueClassMap = columnValueMap.get(value);
		if(valueClassMap == null) {
			valueClassMap = new HashMap<String, Double>();
		}
		
		valueClassMap.put(classValue, probability);
		columnValueMap.put(value, valueClassMap);
		parametes.put(columnNumber, columnValueMap);
	}
}
